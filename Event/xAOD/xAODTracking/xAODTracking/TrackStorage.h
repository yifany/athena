/*
  Copyright (C) 2002-2023 CERN for the benefit of the ATLAS collaboration
*/
#ifndef XAODTRACKING_TRACKSTORAGE_H
#define XAODTRACKING_TRACKSTORAGE_H

#include "xAODTracking/versions/TrackStorage_v1.h"

namespace xAOD {
  typedef TrackStorage_v1 TrackStorage;
}
#endif